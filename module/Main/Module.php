<?php

namespace Main;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Main\Model\Main;
use Main\Model\MainTable;
use Main\Model\Gendep;
use Main\Model\GendepTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'MainModelMainTable' =>  function($sm) {
                    $tableGateway = $sm->get('MainTableGateway');
                    $table = new MainTable($tableGateway);
                    return $table;
                },
                'MainTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Main());
                    return new TableGateway('main', $dbAdapter, null, $resultSetPrototype);
                },
                'MainModelGendepTable' =>  function($sm) {
                    $tableGateway = $sm->get('GendepTableGateway');
                    $table = new GendepTable($tableGateway);
                    return $table;
                },
                'GendepTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Gendep());
                    return new TableGateway('gendep', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
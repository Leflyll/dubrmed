<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;

class OtherController extends AbstractActionController
{

    public $adapter;

    public function __construct()
    {
        $this->adapter = new Adapter(array(
            'driver' => 'Mysqli',
            'database' => 'dubrmed',
            'username' => 'user',
            'password' => ''
        ));
    }

    public function IndexAction()
    {
        $name = $this->params()->fromRoute('name');
        $info = $this->adapter->query("SELECT info1 FROM maininfo WHERE name = '$name';")->execute()->current();
        $container = new Container('auth');
        if ($container->role > 3) {
            $role = $container->role;
            $request = $this->getRequest();
            $data = $request->getPost();
            if (($data['message']) != null) {
                if ($info['info1'] != null) {
                    $sql = new Sql($this->adapter);
                    $update = $sql->update();
                    $update->table('maininfo');
                    $dataforupdate = array(
                        'info1'=>$data['message']
                    );
                    var_dump($data['message']);
                    $update->set($dataforupdate);
                    $update->where(array('name' => $name));
                    $statementUpdate = $sql->getSqlStringForSqlObject($update);

                    var_dump($statementUpdate);
                    $sectorName = $this->adapter->query($statementUpdate,
                        \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
//                    $statement = $sql->prepareStatementForSqlObject($update);
//                    print_r($update->getRawState());
//                    $results = $statement->execute();
                } else {

                }
            }
        } else {
            $role = null;
        }
        $info = $this->adapter->query("SELECT info1 FROM maininfo WHERE name = '$name';")->execute()->current();
        return new ViewModel(array('info' => $info["info1"], 'role' => $role, 'name' => $name));

    }

}
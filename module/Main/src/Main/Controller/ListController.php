<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class ListController extends AbstractActionController
{

//    protected


    public function indexAction()
    {
        $tablename = $this->params()->fromRoute('tablename');
        $id = (int)$this->params()->fromRoute('id');
        $info = $this->adapter->query("SELECT name, info FROM $tablename WHERE id = '$id';");
        if ($info == true) {
            $info = $info->execute();
            $info = $info->current();
        }else{
            $info = 'На жаль, ця сторінка не існує';
        }
        return new ViewModel(array('info' => $info));
    }
}
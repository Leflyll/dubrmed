<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Form\Factory;
use \Zend\Db\Adapter\Adapter;
use \Zend\Authentication\Adapter\DbTable\AuthAdapter;
use Zend\Session\Container;
use \Zend\Validator\EmailAddress;


class IndexController extends AbstractActionController
{
    public $adapter;

    protected $mainTable;
    protected $gendepTable;

    public function __construct()
    {
        $this->adapter = new Adapter(array(
            'driver' => 'Mysqli',
            'database' => 'dubrmed',
            'username' => 'user',
            'password' => ''
        ));
    }


    public function form()
    {
        $factory = new Factory();
        return $form = $factory->createForm(array(
            'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
            'elements' => array(
                array(
                    'spec' => array(
                        'name' => 'name',
                        'options' => array(
                            'label' => "&emsp;&emsp;Ваше ім'я",
                        ),
                        'attributes' => array(
                            'type' => 'text'
                        ),
                    )
                ),
                array(
                    'spec' => array(
                        'type' => 'Zend\Form\Element\Email',
                        'name' => 'email',
                        'options' => array(
                            'label' => '&emsp;&emsp;Ваш електронний адрес',
                        )
                    ),
                ),
                array(
                    'spec' => array(
                        'type' => '\Zend\Form\Element\Password',
                        'name' => 'password',
                        'options' => array(
                            'label' => '&emsp;&emsp;Пароль',
                        ),
                    ),
                ),
                array(
                    'spec' => array(
                        'type' => '\Zend\Form\Element\Password',
                        'name' => 'rpassword',
                        'options' => array(
                            'label' => '&emsp;&emsp;Повторіть пароль',
                        ),
                    ),
                ),
                array(
                    'spec' => array(
                        'name' => 'send',
                        'attributes' => array(
                            'type' => 'submit',
                            'value' => 'Увійти',
                        ),
                    ),
                ),
                array(
                    'spec' => array(
                        'name' => 'logout',
                        'attributes' => array(
                            'type' => 'submit',
                            'value' => 'Вийти',
                        ),
                    ),
                ),
            ),
            /* If we had fieldsets, they'd go here; fieldsets contain
             * "elements" and "fieldsets" keys, and potentially a "type"
             * key indicating the specific FieldsetInterface
             * implementation to use.
            'fieldsets' => array(
            ),
             */

            // Configuration to pass on to
            // ZendInputFilterFactory::createInputFilter()
            'input_filter' => array(/* ... */
            ),
        ));
    }

    public function getMainTable()
    {
        if (!$this->mainTable) {
            $sm = $this->getServiceLocator();
            $this->mainTable = $sm->get('Main\Model\MainTable');
        }
        return $this->mainTable;
    }

    public function getGenDepTable()
    {
        if (!$this->gendepTable) {
            $sm = $this->getServiceLocator();
            $this->gendepTable = $sm->get('Main\Model\GendepTable');
        }
        return $this->gendepTable;
    }


    public function indexAction()
    {
        return new ViewModel();
    }


    public function mainAction()
    {
        $page = (int)$this->params()->fromRoute('id');

        $ItemCountPerPage = 10;

        $paginator = $this->getMainTable()->fetchAll(true);
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $page));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage($ItemCountPerPage);
//        echo '<pre>';
//        var_export($paginator);die;
        if ($page == 1 || $page == null) {
            $prev = null;
        } else {
            $prev = $page - 1;
        };

        return new ViewModel(array(
            'paginator' => $paginator,
            'pageCount' => $paginator->count(),
            'ItemCountPerPage' => $ItemCountPerPage,
            'previous' => $prev,
            'current' => $page,
            'next' => $page,
            'count' => $paginator->getTotalItemCount()
        ));

//        return new ViewModel(array(
//            'albums' => $this->getMainTable()->fetchAll(),
//        ));

//        return new ViewModel();
    }
    
    public function GenDepAction(){
        $page = (int)$this->params()->fromRoute('id');

        $ItemCountPerPage = 10;

        $paginator = $this->getGendepTable()->fetchAll(true);
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int)$this->params()->fromQuery('page', $page));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage($ItemCountPerPage);

        if ($page == 1 || $page == null) {
            $prev = null;
        } else {
            $prev = $page - 1;
        };

        return new ViewModel(array(
            'paginator' => $paginator,
            'pageCount' => $paginator->count(),
            'ItemCountPerPage' => $ItemCountPerPage,
            'previous' => $prev,
            'current' => $page,
            'next' => $page,
            'count' => $paginator->getTotalItemCount()
        ));
    }

    public function SignUpAction()
    {
        $adapter = $this->adapter;


        $validator = new EmailAddress();


        $request = $this->getRequest();
        $data = $request->getPost();
        if ($data['email'] != null) {
            $password = $data['password'];
            $email = $adapter->query('select * from users where email = "' . $data['email'] . '";');

            $email = $email->execute();
            $email = $email->current();
            if ($password == $data['rpassword'] && $password != null && strlen($password) >= 6 && strlen($password) <= 16) {
                if (($validator->isValid($data['email'])) && $data['email'] != $email['email']) {
                    $password = md5($data['password']);
                    $email = $data['email'];
                    $name = $data['name'];
                    $adapter->query("insert into users(name, email, password, role) values ('$name','$email','$password', '1');", Adapter::QUERY_MODE_EXECUTE);
                    $message = 'Реєстрація успішна';
                } else {
                    $message = 'Такий логін уже існує';
                }
            } else {
                $message = 'Проблема з паролем';
            }
        } else {
            $message = 'Реєстрація';
        }


        return new ViewModel(array('form' => $this->form(), 'message' => $message));
    }


    public function LogInAction()
    {
        $session = new Container('auth');

        $request = $this->getRequest();
        $data = $request->getPost();
        if ($data['logout']) {
            $session->name = null;
            $session->role = null;
        }

        $authAdapter = new AuthAdapter($this->adapter, 'users', 'email', 'password', $data['email'], md5($data['password']));
        $info = $authAdapter->authenticate();

        return new ViewModel(array('form' => $this->form(), 'result' => $info, 'session' => $session->name));
    }
    
    public function AdministrationAction(){
        
        $info = $this->adapter->query("SELECT info1 FROM maininfo WHERE name = 'administration';")->execute()->current();
        return new ViewModel(array('info' => $info["info1"]));

    }
    
}
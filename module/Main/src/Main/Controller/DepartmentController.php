<?php

namespace Main\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Zend\Db\Adapter\Adapter;


class DepartmentController extends AbstractActionController
{
    public $adapter;

    public function __construct()
    {
        $this->adapter = new Adapter(array(
            'driver' => 'Mysqli',
            'database' => 'dubrmed',
            'username' => 'user',
            'password' => ''
        ));
    }
    
    public function IndexAction(){
        
        $name = (int)$this->params()->fromRoute('name');
        $info = $this->adapter->query("SELECT info1 FROM maininfo WHERE name = '$name';")->execute()->current();
        return new ViewModel(array('info' => $info["info1"]));

    }
    
}


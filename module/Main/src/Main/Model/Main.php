<?php
namespace Main\Model;

class Main
{
    public $id;
    public $name;
    public $info;

    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->name = (isset($data['name'])) ? $data['name'] : null;
        $this->info  = (isset($data['info'])) ? $data['info'] : null;
    }
}
<?php



return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '[/:action][/:id]/',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Main\Controller\Index',
                        'action'     => 'main'
                    ),
                ),
            ),
            'list' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/list[/:tablename][/:id]/',
                    'constraints' => array(
                        'tablename'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Main\Controller\List',
                        'action' => 'index',
                    ),
                ),
            ),
            'department' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/department[/:name]/',
                    'constraints' => array(
                        'tablename'     => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Main\Controller\Department',
                        'action' => 'index',
                    ),
                ),
            ),
            'other' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/other[/:name]/',
                    'constraints' => array(
                        'tablename'     => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Main\Controller\Other',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Main\Controller\Index' => 'Main\Controller\IndexController',
            'Main\Controller\List' => 'Main\Controller\ListController',
            'Main\Controller\Department' => 'Main\Controller\DepartmentController',
            'Main\Controller\Other' => 'Main\Controller\OtherController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Zend\Db\Adapter\Adapter;
use Zend\Session\Container;
use Zend\Form\Element;
use Zend\Form\Form;


class IndexController extends AbstractActionController {

    public $adapter;
    protected $mainTable;
    protected $gendepTable;

    public function __construct() {
        $this->adapter = new Adapter(array(
            'driver' => 'Mysqli',
            'database' => 'dubrmed',
            'username' => 'user',
            'password' => ''
        ));
        $container = new Container('auth');
        if ($container->role > 3) {
            
        } else {
            die;
        }
    }



    public function getMainTable() {
        if (!$this->mainTable) {
            $sm = $this->getServiceLocator();
            $this->mainTable = $sm->get('Main\Model\MainTable');
        }
        return $this->mainTable;
    }

    public function getGenDepTable() {
        if (!$this->gendepTable) {
            $sm = $this->getServiceLocator();
            $this->gendepTable = $sm->get('Main\Model\GendepTable');
        }
        return $this->gendepTable;
    }

    public function indexAction() {


        return new ViewModel();
    }


    public function formforedit($name)
    {
        $message = new Element\Textarea('message');
        $message->setLabel('Message');
        $info = $this->adapter->query("SELECT info1 FROM maininfo WHERE name = '$name'")->execute()->current();
        $message->setValue($info['info1']);
        $send = new Element('send');
        $send->setValue('Зберегти');
        $send->setAttributes(array(
            'type'  => 'submit'
        ));


        $form = new Form('Edit');
        $form->add($message);
        $form->add($send);
        return $form;
    }

    public function editAction() {
        $name = $this->params()->fromRoute('name');
        return new ViewModel(array('name' => $name, 'form' => $this->formforedit($name)));
    }

}

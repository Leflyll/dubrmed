<?php

namespace Zend\Authentication\Adapter\DbTable;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Session\Container;

class AuthAdapter implements AdapterInterface
{

    public $DbAdapter;
    public $TableName;
    public $emailCol;
    public $passwordCol;
    public $email;
    public $password;

    public function __construct($DbAdapter, $TableName, $emailCol, $passwordCol, $email, $password)
    {
        $this->DbAdapter=$DbAdapter;
        $this->TableName=$TableName;
        $this->emailCol=$emailCol;
        $this->passwordCol=$passwordCol;
        $this->email=$email;
        $this->password=$password;
    }

    public function authenticate()
    {
        if ($this->select()!= null){
            $data = $this->select();
            if ($this->email == $data['email'] && $this->password == $data['password']){
                $container = new Container('auth');
                $container->name = $this->email;
                $container->role = $data['role'];
                return array('isset'=>true, 'isValid' => true);
            }else{
                return array('isset'=>true, 'isValid' => false);
            }
        }else{
            return array('isset'=>false, 'isValid' => false);
        }
    }

    public function select()
    {
        if ($this->email != null) {
            $data = $this->DbAdapter->query("select $this->emailCol, $this->passwordCol, role from $this->TableName where email = '$this->email'");
            if ($data->execute()->current() != null) {
                $data = $data->execute()->current();
                return array('email'=>$data['email'], 'password'=>$data['password'], 'role'=>$data['role']);
            } else {
                return null;
            }
        }
    }

}